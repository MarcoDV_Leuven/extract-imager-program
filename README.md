# Extract detections and program from Imager file
## Data must be an Imager tif file

Imager saves the Imager programs in the metadata of the saved tif file. It mostly consists of:
1. The defined detections of the experiment: LEDs, filters, cameras settings
2. The defined acquisition program: stage loops, time series,  irradiations etc..

Currently the only way to read the program file is to open it with Imager and read the program.

## Solution
This script allows the direct export to disk of the detections and program on any Imager-created tif.

1. It will create a detections.csv file containing all the defined detections in a readable and filterable fashion
2. It will create a program.txt file containing the program in json format but still in a more readable format.

## Usage
1. Install conda as described [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)
2. Create a conda environment in which python and all dependencies will be install automatically by conda using:

```
conda env create -f env.yml
```

3. Activate the created environment with:

```
conda activate python_base
```

4. Use the script like described below:

## Functionality

Once you have a functional installation of Python, make sure to be in the folder containing the extract_program.py file and use:
```
python extract_program.py -tag "path/to/image.tif"
```
Where -tag can be:

1. -e for essential settings only (detection without many  camera settings)
2. -f for full settings (all camera settings and imager acquisition program)

